<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DatabaseController;
use App\Http\Controllers\DataKeluargaController;
use App\Http\Controllers\DataLainController;
use App\Http\Controllers\DataPesertaDidikController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\InformasiController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\PengumumanController;
use App\Http\Controllers\PesanController;
use App\Models\Pengumuman;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::middleware('auth')->group(function () {
    Route::get('/admin', [DashboardController::class, 'index']);
    Route::resource('/admin/informasi', InformasiController::class);
    Route::resource('/admin/pengumuman', PengumumanController::class);
    Route::get('/pendaftaran/{id}', [DashboardController::class, 'pendaftaran']);
    Route::post('/daftar_ulang/{id}', [DashboardController::class, 'daftar_ulang']);
    Route::get('/detil/{id}', [FormController::class, 'detil']);
    Route::get('admin/pesan_1', [PesanController::class, 'pesan_1']);
    Route::get('admin/pesan_1', [PesanController::class, 'pesan_1']);
    Route::post('pesan/update/{id}', [PesanController::class, 'update_pesan']);

    Route::get('sent_wa/{id}', [DashboardController::class,'sent_wa']);

    Route::get('export_data', [LaporanController::class,'export_data']);
});

Route::get('/', [FormController::class, 'index']);
Route::resource('/form', FormController::class);
Route::get('/cetak_form/{id}', [FormController::class, 'cetak_form']);
Route::get('informasi', [InformasiController::class, 'tampil']);
Route::get('pengumuman', [PengumumanController::class, 'tampil']);
Route::get('ada', [PesanController::class, 'ada']);



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
