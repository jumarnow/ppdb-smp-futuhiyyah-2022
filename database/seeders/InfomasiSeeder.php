<?php

namespace Database\Seeders;

use App\Models\Informasi;
use Illuminate\Database\Seeder;

class InfomasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = new Informasi();
        $data->isi = '<p><strong>Informasi Pendaftaran SMP FUTUHIYYAH MRANGGEN</strong></p><p>Daftar Gelombang :</p><ol><li>Gelombang 1 = 1 - 30 Maret 2022</li></ol>';
        $data->save();
    }
}
