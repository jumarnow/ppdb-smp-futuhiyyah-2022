<?php

namespace Database\Seeders;

use App\Models\DataKeluarga;
use App\Models\DataLain;
use App\Models\DataPesertaDidik;
use Illuminate\Database\Seeder;
use Symfony\Component\Mime\Part\DataPart;

class DataPesertaDidikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i=5; $i < 20; $i++) { 
            $data = new DataPesertaDidik();
            $data->id = $i;
            $data->nama_lengkap = 'Siswa '.$i;
            $data->jenis_kelamin = 'L';
            $data->nisn = '123'.$i;
            $data->nik = '12345'.$i;
            $data->no_hp = '+62 878-4914-9425';
            $data->nama_sekolah_asal = 'MI Futuhiyyah';
            $data->sent = 'Terkirim';
            $data->pendaftaran = 'pendaftaran';
            $data->daftar_ulang = 1000000;
            $data->save();

            $data = new DataKeluarga();
            $data->nama_ayah = 'Agus';
            $data->nama_ibu = 'Dewi';
            $data->siswa_id = $i;
            $data->save();

            $data = new DataLain();
            $data->tinggi_badan = '155';
            $data->berat_badan = '50';
            $data->siswa_id = $i;
        }
    }
}