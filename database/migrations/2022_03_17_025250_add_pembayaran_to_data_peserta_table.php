<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPembayaranToDataPesertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_peserta_didiks', function (Blueprint $table) {
            $table->string('pendaftaran')->nullable();
            $table->string('daftar_ulang')->nullable();
            $table->string('sent')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_peserta_didiks', function (Blueprint $table) {
            $table->dropColumn('pendaftaran');
            $table->dropColumn('daftar_ulang');
        });
    }
}
