<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class GlobalExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($return)
    {
        $this->return = $return;
    }

    public function view(): View {
        $return = $this->return;
        return $return;
    }
}
