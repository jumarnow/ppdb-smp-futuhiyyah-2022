<?php

namespace App\Http\Controllers;

use App\Models\DataPesertaDidik;
use App\Models\Pesan;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    public function index(Request $request){
        $limit = 10;
        $data_siswa = DataPesertaDidik::all();
        $siswa = DataPesertaDidik::select("*");
        if ($request->nama) {
            $siswa = $siswa->where('nama_lengkap', 'like', '%'.$request->nama.'%');
        }
        if ($request->bayar == 'pendaftaran') {
            $siswa = $siswa->whereNotNull('pendaftaran');
        }
        if ($request->bayar == 'daftar_ulang') {
            $siswa = $siswa->whereNotNull('daftar_ulang');
        }
        if ($request->limit) {
            $limit = $request->limit;
        }
        $siswa = $siswa->orderBy('created_at','desc')->paginate($limit);
        return view('dashboard', compact('siswa','data_siswa'));
    }

    public function pendaftaran($id){
        // $pesan = Pesan::find(1);
        // dd($pesan->isi);
        $data = DataPesertaDidik::find($id);
        $data->pendaftaran = 'pendaftaran';
        $data->save();
        $this->sent_wa($id);
        $no_hp = preg_replace('/[^0-9]/', '', $data->no_hp);
        // return back()->with('simpan', 'Data berhasil disimpan');
        if (substr($no_hp,0,2) == "08") {
            return redirect('https://wa.me/62'.substr($no_hp,1)."?text=Berdasarkan keputusan Panitia Peserta Didik Baru SMP FUTUHIYYAH Mranggen Demak, dengan ini memutuskan bahwa:%0D%0A%0D%0A Nama :  $data->nama_lengkap %0D%0AAsal Sekolah :  $data->nama_sekolah_asal %0D%0A ==*DITERIMA*== %0D%0A%0D%0A Sebagai siswa%2Fsiswi SMP FUTUHIYYAH Mranggen Demak pada tahun pelajaran 2022%2F2023. %0D%0ASelanjutnya dimohon segera melakukan daftar ulang dan melengkapi berkas pendaftaran. Untuk rincian daftar ulang dapat dilihat di link berikut https://ppdbv2.smpfutuhiyyah.sch.id/public/daful_smpf_22.jpeg");
        }elseif(substr($no_hp,0,2) == "62"){
            return redirect('https://wa.me/'.$no_hp."?text=Berdasarkan keputusan Panitia Peserta Didik Baru SMP FUTUHIYYAH Mranggen Demak, dengan ini memutuskan bahwa:%0D%0A%0D%0ANama :  $data->nama_lengkap %0D%0AAsal Sekolah :  $data->nama_sekolah_asal %0D%0A ==*DITERIMA*== %0D%0A%0D%0A Sebagai siswa%2Fsiswi SMP FUTUHIYYAH Mranggen Demak pada tahun pelajaran 2022%2F2023. %0D%0ASelanjutnya dimohon segera melakukan daftar ulang dan melengkapi berkas pendaftaran. Untuk rincian daftar ulang dapat dilihat di link berikut https://ppdbv2.smpfutuhiyyah.sch.id/public/daful_smpf_22.jpeg ");
            // return redirect('https://wa.me/'.$no_hp."?text=".$pesan->isi);
        }else{
            dd('Nomor '.$data->no_hp.' tidak dikenali');
        }
    }

    public function daftar_ulang(Request $request, $id){
        // dd($request->all());
        $nominal = preg_replace("/[^0-9]/", "", $request->nominal);
        $data = DataPesertaDidik::find($id);
        $data->daftar_ulang = $nominal;
        $data->save();
        return back()->with('simpan', 'Data berhasil disimpan');
    }

    public function sent_wa($id){
        $data = DataPesertaDidik::find($id);
        $data->sent = 'Terkirim';
        $data->save();

        return response()->json($data, 200);
    }

    public function get_student(){
        $student = DataPesertaDidik::with('keluarga','lain')->whereNotNull('daftar_ulang')->get();
        return response()->json($student, 200);
    }
}
