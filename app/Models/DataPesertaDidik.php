<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataPesertaDidik extends Model
{
    use HasFactory, SoftDeletes;

    public function keluarga(){
        return $this->hasOne(DataKeluarga::class, 'siswa_id');
    }

    public function lain(){
        return $this->hasOne(DataLain::class, 'siswa_id');
    }
}
