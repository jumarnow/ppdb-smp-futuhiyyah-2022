@extends('layouts.admin')
@section('content')
            
    <section class="content-header">
        <h1>
            <i class='fa fa-users'></i> Detil Pendaftar &nbsp;&nbsp;
            {{-- <a href='javascript:showUpload();' id='import' class='btn btn-success btn-sm'><i class='fa fa-upload'></i> Import</a> --}}
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Halaman Awal</a></li>
            <li class="active">Detil Pendaftar</li>
        </ol>
    </section>

    <!-- Main content -->
    <section id='content_section' class="content">
    <!-- Your Page Content Here -->
        
    <div>

    <div class="row" style="margin-bottom: 10px">
        <div class="col-sm-6">
            <div style="padding-top: 5px">
                <p><a title='Return' href="{{url('/admin')}}">
                    <i class='fa fa-chevron-circle-left '></i>&nbsp; Kembali ke halaman pendaftar</a>
                </p>
            </div>
        </div>
        <div class="col-sm-6">
            <div align="right"></div>
        </div>
    </div>

    <div class="panel panel-default" style="margin-bottom: 10px">
        <div class="panel-heading">
            <strong><i class='fa fa-users'></i> Data Siswa</strong>
        </div>
        <div class="panel-body" style="padding:20px 0px 0px 0px">
            <div class="box-body" id="parent-form-area">
                
                <div class='table-responsive'>
                    <table id='' class='table table-striped'>
                        <tr>
                            <td style="width: 200px">Nama Siswa</td>
                            <td>{{$siswa->nama_lengkap}}</td>
                        </tr>
                        <tr>
                            <td>Jenis Kelamin</td>
                            <td>{{$siswa->jenis_kelamin}}</td>
                        </tr>
                        <tr>
                            <td>NISN</td>
                            <td>{{$siswa->nisn}}</td>
                        </tr>
                        <tr>
                            <td>NIK</td>
                            <td>{{$siswa->nik}}</td>
                        </tr>
                        <tr>
                            <td>Nama Sekolah Asal</td>
                            <td>{{$siswa->nama_sekolah_asal}}</td>
                        </tr>
                        <tr>
                            <td>Alamat Sekolah Asal</td>
                            <td>{{$siswa->alamat_sekolah_asal}}</td>
                        </tr>
                        <tr>
                            <td>TTL</td>
                            <td>{{$siswa->tempat_lahir}}, {{$siswa->tanggal}}</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>{{$siswa->alamat}}, {{$siswa->kelurahan}}, {{$siswa->kecamatan}}, {{$siswa->kabupaten}}, {{$siswa->provinsi}}</td>
                        </tr>
                        <tr>
                            <td>Transportasi</td>
                            <td>{{$siswa->transportasi}}</td>
                        </tr>
                        <tr>
                            <td>No HP</td>
                            <td>{{$siswa->no_hp}}</td>
                        </tr>
                    </table>
                </div>
            
            </div><!-- /.box-body -->
        </div>
    </div>

    <div class="panel panel-default" style="margin-bottom: 10px">
        <div class="panel-heading">
            <strong><i class='fa fa-users'></i> Data Keluarga</strong>
        </div>
        <div class="panel-body" style="padding:20px 0px 0px 0px">
            <div class="box-body" id="parent-form-area">
                
                <div class='table-responsive'>
                    <table id='' class='table table-striped'>
                        <tr>
                            <td style="width: 200px">Nama Ayah</td>
                            <td>{{$siswa->keluarga->nama_ayah}}</td>
                        </tr>
                        <tr>
                            <td>Pekerjaan Ayah</td>
                            <td>{{$siswa->keluarga->pekerjaan_ayah}}</td>
                        </tr>
                        <tr>
                            <td>Pendidikan Ayah</td>
                            <td>{{$siswa->keluarga->pendidikan_ayah}}</td>
                        </tr>
                        <tr>
                            <td>Penghasilan Ayah</td>
                            <td>{{$siswa->keluarga->penghasilan_ayah}}</td>
                        </tr>
                        <tr>
                            <td style="width: 200px">Nama Ibu</td>
                            <td>{{$siswa->keluarga->nama_ibu}}</td>
                        </tr>
                        <tr>
                            <td>Pekerjaan Ibu</td>
                            <td>{{$siswa->keluarga->pekerjaan_ibu}}</td>
                        </tr>
                        <tr>
                            <td>Pendidikan Ibu</td>
                            <td>{{$siswa->keluarga->pendidikan_ibu}}</td>
                        </tr>
                        <tr>
                            <td>Penghasilan Ibu</td>
                            <td>{{$siswa->keluarga->penghasilan_ibu}}</td>
                        </tr>
                    </table>
                </div>
            
            </div><!-- /.box-body -->
        </div>
    </div>

    <div class="panel panel-default" style="margin-bottom: 10px">
        <div class="panel-heading">
            <strong><i class='fa fa-users'></i> Data Lain</strong>
        </div>
        <div class="panel-body" style="padding:20px 0px 0px 0px">
            <div class="box-body" id="parent-form-area">
                
                <div class='table-responsive'>
                    <table id='' class='table table-striped'>
                        <tr>
                            <td style="width: 200px">Nam Pondok</td>
                            <td>{{$siswa->lain->nama_pondok}}</td>
                        </tr>
                        <tr>
                            <td>Tinggi Badan / Berat Badan</td>
                            <td>{{$siswa->lain->tinggi_badan}} / {{$siswa->lain->berat_badan}}</td>
                        </tr>
                        <tr>
                            <td>Jarak ke Sekolah</td>
                            <td>{{$siswa->lain->jarak_sekolah}}</td>
                        </tr>
                        <tr>
                            <td>Waktu ke Sekolah</td>
                            <td>{{$siswa->lain->waktu_sekolah}}</td>
                        </tr>
                        <tr>
                            <td style="width: 200px">Anak Ke / Jumlah Saudara</td>
                            <td>{{$siswa->lain->anak_ke}} / {{$siswa->lain->jumlah_saudara}}</td>
                        </tr>
                    </table>
                </div>
            
            </div><!-- /.box-body -->
        </div>
    </div>
    </div><!--END AUTO MARGIN-->
@endsection