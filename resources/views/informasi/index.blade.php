@extends('layouts.admin')
@section('content')
    	
	<section class="content-header">
	  <h1>
		Informasi Pendaftaran
		{{-- <small>Control panel</small> --}}
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Informasi Pendaftaran</li>
	  </ol>
	</section>

	<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {{-- <div class="pull-right box-tools"> --}}
                    <div class="box-body pad">
                        <form action="{{ route('informasi.update', $informasi->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <textarea required id="editor" name="isi" rows="10" cols="80">
                                {{ $informasi->isi }}
                            </textarea>
                            <br>
                            <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-send"></i> Simpan</button>
                        </form>
                    </div>
              {{-- </div> --}}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('js')
<script>

    CKEDITOR.replace('editor')
</script>
@endsection