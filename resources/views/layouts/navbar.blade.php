<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
    <div class="pull-left image">
        <img src="{{ url('/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
        <p>Admin</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
            </span>
    </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
    <li class="header">MENU UTAMA</li>
    <li class="{{ Request::is('admin') ? 'active' : '' }}">
        <a href="{{ url('admin') }}">
        <i class="fa fa-dashboard"></i> <span>Data Pendaftar</span>
        <span class="pull-right-container">
            {{-- <small class="label pull-right bg-green">new</small> --}}
        </span>
        </a>
    </li>
    <li class="{{ Request::is('admin/informasi') ? 'active' : '' }}">
        <a href="{{ url('admin/informasi') }}">
        <i class="fa fa-circle-o"></i> <span>Informasi</span>
        <span class="pull-right-container">
            {{-- <small class="label pull-right bg-green">new</small> --}}
        </span>
        </a>
    </li>
    <li class="{{ Request::is('admin/pengumuman') ? 'active' : '' }}">
        <a href="{{ url('admin/pengumuman') }}">
        <i class="fa fa-circle-o"></i> <span>Pengumuman</span>
        <span class="pull-right-container">
            {{-- <small class="label pull-right bg-green">new</small> --}}
        </span>
        </a>
    </li>
    <li class="treeview">
        <a href="#">
        <i class="fa fa-th"></i> <span>Master Data</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
            <li class="{{ Request::is('admin/sekolah') ? 'active' : '' }}">
                <a href="{{ url('admin/sekolah') }}">
                <i class="fa fa-circle-o"></i> <span>Data Sekolah</span>
                <span class="pull-right-container">
                    <small class="label pull-right bg-green">new</small>
                </span>
                </a>
            </li>
            <li class="{{ Request::is('admin/pesan_1') ? 'active' : '' }}">
                <a href="{{ url('admin/pesan_1') }}">
                <i class="fa fa-circle-o"></i> <span>Pesan 1</span>
                <span class="pull-right-container">
                    <small class="label pull-right bg-green">new</small>
                </span>
                </a>
            </li>
            <li class="{{ Request::is('admin/pesan_2') ? 'active' : '' }}">
                <a href="{{ url('admin/pesan_2') }}">
                <i class="fa fa-circle-o"></i> <span>Pesan 2</span>
                <span class="pull-right-container">
                    <small class="label pull-right bg-green">new</small>
                </span>
                </a>
            </li>
        </ul>
    </li>
    </ul>
</section>