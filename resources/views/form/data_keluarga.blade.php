<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Data Keluarga</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{-- <form role="form" action="{{ route('data_keluarga.store') }}"> --}}
        <div class="form-horizontal">
        <div class="box-body">
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Nama Ayah <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <input type="text" required class="form-control" name="nama_ayah" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Pekerjaan Ayah <span style="color: red">*</span> </label>
            <div class="col-sm-10">
            <input type="text" required class="form-control" name="pekerjaan_ayah" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Pendidikan Ayah <span style="color: red">*</span> </label>
            <div class="col-sm-10">
            <select name="pendidikan_ayah" required class="form-control" id="">
                <option value="">**Pilih Pendidikan</option>
                <option>Tidak Sekolah</option>
                <option>SD</option>
                <option>SLTP</option>
                <option>SLTA</option>
                <option>D3</option>
                <option>S1</option>
                <option>S2</option>
                <option>S3</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Penghasilam Ayah </label>
            <div class="col-sm-10">
            <select name="penghasilan_ayah" class="form-control" id="">
                <option value="">**Pilih Penghasilan</option>
                <option>Tidak Berpenghasilan</option>
                <option>< 500.000</option>
                <option>500.000 - 1.000.000</option>
                <option>1.000.000 - 3.000.000</option>
                <option>3.000.000 - 5.000.000</option>
                <option>> 5.000.000</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Nama Ibu <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <input type="text" required class="form-control" name="nama_ibu" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Pekerjaan Ibu <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <input type="text" required class="form-control" name="pekerjaan_ibu" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Pendidikan Ibu <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <select name="pendidikan_ibu" required class="form-control" id="">
                <option value="">**Pilih Pendidikan</option>
                <option>Tidak Sekolah</option>
                <option>SD</option>
                <option>SLTP</option>
                <option>SLTA</option>
                <option>D3</option>
                <option>S1</option>
                <option>S2</option>
                <option>S3</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Penghasilam Ibu </label>
            <div class="col-sm-10">
            <select name="penghasilan_ibu" class="form-control" id="">
                <option value="">**Pilih Penghasilan</option>
                <option>Tidak Berpenghasilan</option>
                <option>< 500.000</option>
                <option>500.000 - 1.000.000</option>
                <option>1.000.000 - 3.000.000</option>
                <option>3.000.000 - 5.000.000</option>
                <option>> 5.000.000</option>
            </select>
            </div>
        </div>
        
        
        </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        {{-- <button type="submit" class="btn btn-primary">Simpan</button> --}}
        </div>
    {{-- </form> --}}
</div>