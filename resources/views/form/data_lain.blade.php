<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Data Lain</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{-- <form role="form" action="{{ route('data_keluarga.store') }}"> --}}
        <div class="form-horizontal">
        <div class="box-body">
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Rencana Pondok Pesantren</label> <br>
            <div class="col-sm-10">
                <input type="radio" name="pondok" value="ya" > Ya
                <input type="radio" name="pondok" value="tidak"> Tidak
                <select name="nama_pondok" class="nama_pondok form-control" id="">
                    <option value="">Pilih Pondok</option>
                    <option>Futuhiyyah</option>
                    <option>Al Mubarok</option>
                    <option>Darussa'adah</option>
                    <option>Al Badriyah</option>
                    <option>Al Amin</option>
                    <option>Nurul Burhani 1</option>
                    <option>Nurul Burhani 2</option>
                    <option>Darul Ma'wa</option>
                    <option>KH Murodi</option>
                    <option>Al Anwar</option>
                    <option>Al Imdad</option>
                    <option>Lainnya</option>
                </select>
                <div class="pondok_lain"></div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Tinggi Badan (cm), Berat Badan (kg)</label>
            <div class="col-sm-10">
            <div class="form-inline">
                <input type="number" name="tinggi_badan" class="form-control" id="" placeholder="Tinggi Badan">
                <input type="number" name="berat_badan" class="form-control" id="" placeholder="Berat Badan">
            </div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Jarak ke sekolah (km) </label>
            <div class="col-sm-10">
            <input type="number" class="form-control" name="jarak_kesekolah" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Waktu ke sekolah (menit) </label>
            <div class="col-sm-10">
            <input type="number" class="form-control" name="waktu_kesekolah" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Anak Ke, Jumlah Saudara <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <div class="form-inline">
                <input type="number" required name="anak_ke" class="form-control" id="" placeholder="Anak Ke">
                <input type="number" required name="jumlah_saudara" class="form-control" id="" placeholder="Jumlah Saudara">
            </div>
            </div>
        </div>

        </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        {{-- <button type="submit" class="btn btn-primary pull-right"><i class="fa-send"></i> Kirim Formulir</button> --}}
        </div>
    {{-- </form> --}}
</div>