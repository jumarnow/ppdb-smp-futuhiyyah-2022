<!DOCTYPE html>
<html>
<head>
	<title>Formulir Penerimaan Peserta Didik Baru</title>
	<style type="text/css">
		table {
			border-style: double;
			border-width: 3px;
			border-color: white;
		}
		table tr .text2 {
			text-align: right;
			font-size: 15px;
		}
		table tr .text {
			text-align: center;
			font-size: 15px;
		}
		table tr td {
			font-size: 15px;
		}

		footer {
			position: fixed;
			bottom: 0;
			width: 100%;
			/* text-align: center; */
		}

	</style>
</head>
<body>
	<table>
		<tr>
			<td><img src="logo-icon.png" width="90" height="90"></td>
			<td>
			<center>
				<font size="4">YAYASAN PONDOK PESANTREN FUTUHIYYAH</font><br>
				<font size="5"><b>SMP FUTUHIYYAH</b></font><br>
				<font size="2">Jl. Suburan Mranggen Demak 59567</font><br>
				<font size="2"><i>Email : futuhiyyahsmp@yahoo.co.id | Website : www.smpfutuhiyyah.sch.id | Telp. (024) 6708202</i></font>
			</center>
			</td>
		</tr>
		<tr>
			<td colspan="2"><hr></td>
		</tr>
	</table>

	<h3 align="center">Formulir Penerimaan Peserta Didik Baru Tahun 2022</h3>
	<table class="">
		<tr>
			<td><b>Registrasi Calon Peserta Didik</b> </td>
			<td></td>
		</tr>
		<tr>
			<td align="right">No Pendaftaran</td>
			<td>: </td>
		</tr>
		<tr>
			<td align="right">No Formulir</td>
			<td>: PPDB2021{{ $siswa->id }}</td>
		</tr>
		<tr>
			<td align="right">Waktu Pendaftaran</td>
			<td>: {{ Carbon\Carbon::parse($siswa->created_at)->isoFormat('DD MMMM Y H:m:s') }}</td>
		</tr>
		<tr>
			<td align="right">Gelombang Pendaftaran</td>
			@php
				$now = Carbon\Carbon::parse($siswa->created_at)->format('m');
			@endphp
			<td>: 
				@if ($now <= 4) Gelombang 1 @endif
				@if ($now == 5 || $now == 6) Gelombang 2 @endif
				@if ($now >= 7) Gelombang 3 @endif
			</td>
		</tr>
		<tr>			
			<td align="right">Nama Sekolah Asal</td>
			<td>: {{ $siswa->nama_sekolah_asal }}</td>
		</tr>
		<tr>
			<td align="right">Alamat Sekolah Asal</td>
			<td>: {{ $siswa->alamat_sekolah_asal }}</td>
		</tr>
		<tr>
			<td><b>Biodata Calon Peserta Didik</b> </td>
			<td></td>
		</tr>
		<tr>
			<td align="right">Nama Lengkap</td>
			<td>: {{ $siswa->nama_lengkap }}</td>
		</tr>
		<tr>
			<td align="right">Jenis Kelamin</td>
			<td>: {{ $siswa->jenis_kelamin }}</td>
		</tr>
		<tr>
			<td align="right">NISN</td>
			<td>: {{ $siswa->nisn }}</td>
		</tr>
		<tr>
			<td align="right">NIK</td>
			<td>: {{ $siswa->nik }}</td>
		</tr>
		<tr>
			<td align="right">Tempat Lahir</td>
			<td>: {{ $siswa->tempat_lahir }}</td>
		</tr>
		<tr>
			<td align="right">Tanggal Lahir</td>
			<td>: {{ $siswa->tanggal_lahir }}</td>
		</tr>
		<tr>
			<td align="right">Agama</td>
			<td>: {{ $siswa->agama }}</td>
		</tr>
		<tr>
			<td align="right">Rencana Pondok</td>
			<td>: @if(isset($siswa->lain->nama_pondok)) {{ $siswa->lain->nama_pondok }} @endif</td>
		</tr>
		<tr>
			<td align="right">No HP</td>
			<td>: {{ $siswa->no_hp }}</td>
		</tr>
		<tr>
			<td><b>Alamat Peserta Didik</b> </td>
			<td></td>
		</tr>
		<tr>
			<td align="right">Alamat Jalan RT/RW</td>
			<td>: {{ $siswa->alamat }}</td>
		</tr>
		<tr>
			<td align="right">Provinsi</td>
			<td>: {{ $siswa->provinsi }}</td>
		</tr>
		<tr>
			<td align="right">Kabupaten / Kota</td>
			<td>: {{ $siswa->kabupaten }}</td>
		</tr>
		<tr>
			<td align="right">Kecamatan</td>
			<td>: {{ $siswa->kecamatan }}</td>
		</tr>
		<tr>
			<td align="right">Kelurahan</td>
			<td>: {{ $siswa->kelurahan }}</td>
		</tr>
	</table>
	<br>
	Saya yang bertandatangan dibawah ini menyatakan bahwa data yang tertera diatas adalah yang sebenarnya.
	<table>
		<tr>
			<td style="width: 450px"></td>
			<td>Demak, {{ Carbon\Carbon::now()->isoFormat('DD MMMM Y') }}</td>
		</tr>
		<br><br><br>
		<tr>
			<td style="width: 450px"></td>
			<td>{{ $siswa->nama_lengkap }}</td>
		</tr>
	</table>

	<footer>

		<hr>
		NB : Harap bawa formulir ini saat mendaftar
	</footer>

</body>
</html>