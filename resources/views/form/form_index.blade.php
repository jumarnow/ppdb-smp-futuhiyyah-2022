@extends('layouts.siswa')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Formulir Pendaftaran Peserta Didik Baru
        {{-- <small>Preview</small> --}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Formulir Pendaftaran Peserta Didik Baru</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      @if (Session::get('error'))
      <div class="row">
          <div class="alert alert-warning alert-dismissible" role="alert">
              <strong><i class="fa fa-info-circle"></i> {{ session('error') }}</strong>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
      </div>
      @endif

      <div class="callout callout-info">
        <h4>Perhatian!</h4>
        1. Isikan formulir dengan benar dan lengkap <br>
        2. Bagian yang Bertanda <span style="color: red"><b> *</b></span> wajib diisi
      </div>
      <div class="row">
        <form action="{{ route('form.store') }}" method="post">
          @csrf
          
        <div class="data_nama"></div>
        <div class="col-md-12">
          @include('form.data_peserta_didik')
        </div>
        <div class="col-md-12">
          @include('form.data_keluarga')
          @include('form.data_lain')
        </div>
        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-send"></i> Kirim Formulir</button>
      </div>
      </form>
    </section>
@endsection
@section('js')
<script>
  $.get("https://dev.farizdotid.com/api/daerahindonesia/provinsi", function(data){
      // console.log(data.provinsi);
      $.each(data.provinsi, function( key, value ){
        $('#provinsi').append("<option value='"+value.id+"'>"+value.nama+"</option>");
      });
  });
  
  $("#provinsi").change(function(){
    var id_provinsi = $(this).val()
    $.get("https://dev.farizdotid.com/api/daerahindonesia/provinsi/"+id_provinsi, function(data){
    $('.data_nama').append('<input type="hidden" name="provinsi" value="'+data.nama+'">')
    });
    $.get("https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi="+id_provinsi, function(data){
      // console.log(data.kota_kabupaten);
      $('#kabupaten').html('')
      $('#kabupaten').append("<option value=''>Pilih Kabupaten</option>");
      $.each(data.kota_kabupaten, function( key, value ){
        $('#kabupaten').append("<option value='"+value.id+"'>"+value.nama+"</option>");
      });
    });
  })
  
  $("#kabupaten").change(function(){
    var id_kota = $(this).val()
    $.get("https://dev.farizdotid.com/api/daerahindonesia/kota/"+id_kota, function(data){
    $('.data_nama').append('<input type="hidden" name="kabupaten" value="'+data.nama+'">')
    });
    $.get("https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota="+id_kota, function(data){
      $('#kecamatan').html('')
      $('#kecamatan').append("<option value=''>Pilih Kecamatan</option>");
      $.each(data.kecamatan, function( key, value ){
      // console.log(value);
        $('#kecamatan').append("<option value='"+value.id+"'>"+value.nama+"</option>");
      });
    });
  })

  $("#kecamatan").change(function(){
    var id_kecamatan = $(this).val()
    $.get("https://dev.farizdotid.com/api/daerahindonesia/kecamatan/"+id_kecamatan, function(data){
    $('.data_nama').append('<input type="hidden" name="kecamatan" value="'+data.nama+'">')
    });
    $.get("https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan="+id_kecamatan, function(data){
      $('#kelurahan').html('')
      $('#kelurahan').append("<option value=''>Pilih Kelurahan</option>");
      $.each(data.kelurahan, function( key, value ){
      // console.log(value);
        $('#kelurahan').append("<option value='"+value.id+"'>"+value.nama+"</option>");
      });
    });
  })

  $("#kelurahan").change(function(){
    var id_kelurahan = $(this).val()
    $.get("https://dev.farizdotid.com/api/daerahindonesia/kelurahan/"+id_kelurahan, function(data){
    // $('.data_nama').append('<input type="hidden" name="kelurahan" value="'+data.nama+'">')
    });
  })
  
  $('.nama_pondok').hide()

  $('input[type=radio][name=pondok]').change(function() {
    if (this.value == 'ya') {
      $('.nama_pondok').show()
    }else{
      $('.nama_pondok').hide()
      $('.pondok_lain').html('')
    }
  });
  
  $('.nama_pondok').change(function(){
    if($(this).val() == 'Lainnya'){
      $('.pondok_lain').append('<input type="text" name="nama_pondok" class="form-control" id="" placeholder="Ketikkan Nama Pondok">')
    }else{
      $('.pondok_lain').html('')
    }
  })

  $('.panjang_nik').change(function(){
    $val = $(this).val();
    if ($val.length != 16) {
        alert('Panjang NIK Harus 16 Digit');
        $(this).focus()
    }
  });

  $('.panjang_nisn').change(function(){
    $val = $(this).val();
    if ($val.length != 10) {
        alert('Panjang NISN Harus 10 Digit');
        $(this).focus()
    }
  });
</script>
@endsection