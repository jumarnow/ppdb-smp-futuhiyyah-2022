@extends('layouts.admin')
@section('content')
	
  @php
      function rupiah($num) {
          return number_format($num, 0, ',', '.');
      }
  @endphp

	<section class="content-header">
	  <h1>
		Data Pendaftar
		{{-- <small>Control panel</small> --}}
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	  </ol>
	</section>

	<!-- Main content -->
    <section class="content">
      @php
          $now = Carbon\Carbon::now()->format('Y-m-d');
      @endphp

      @if ( $now < '2022-05-31' )
      <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-info-circle"></i> Isi nominal pembayaran daftar ulang!</h4>
        Silahkan isi nominal daftar ulang dengan cara klik tombol daftar ulang setiap pendaftar yang sudah melakukan daftar ulang
      </div>
      @endif

      <div class="modal fade" id="pendaftaran">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Konfirmasi Pembayaran</h4>
            </div>
            <div class="modal-body">
              <p>Apakah anda yakin akan mengkonfirmasi pembayaran tersebut!</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <a target="blank" type="button" class="btn btn-primary btn_konfirmasi">Konfirmasi</a>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="daftarUlang">
        <div class="modal-dialog">
          <div class="modal-content">
            <form action="" class="form_konfirmasi" method="post">
              @csrf
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Konfirmasi Pembayaran</h4>
            </div>
            <div class="modal-body">
              {{-- <p>Apakah anda yakin akan mengkonfirmasi pembayaran tersebut!</p> --}}
              <label for="">Nominal Pembayaran :</label>
              <input type="text" required class="mata_uang form-control" name="nominal">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Konfirmasi</button>
            </div>
            </form>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          {{-- <div class="small-box bg-aqua"> --}}
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ count($data_siswa) }}</h3>

              <p>Semua Pendaftar</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ count($data_siswa->whereNotNull('pendaftaran')) }}</h3>

              <p>Pendaftaran Terbayar</p>
            </div>
            <div class="icon">
              <i class="ion ion-cash"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ count($data_siswa->whereNotNull('daftar_ulang')) }}</h3>

              <p>Daftar Ulang Terbayar</p>
            </div>
            <div class="icon">
              <i class="ion ion-cash"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        {{-- <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div> --}}
        <!-- ./col -->
      </div>
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
              <form action="" method="get" class="form-inline">
                <input type="text" name="nama" class="form-control" placeholder="Cari Siswa" value="{{ Request::input('nama') }}">
                <select name="bayar" class="form-control" id="">
                  <option value="">Semua</option>  
                  <option {{ Request::input('bayar') == 'pendaftaran' ? 'selected' : '' }} value="pendaftaran">Pendaftaran</option>  
                  <option {{ Request::input('bayar') == 'daftar_ulang' ? 'selected' : '' }} value="daftar_ulang">Daftar Ulang</option>  
                </select>
                <select name="limit" class="form-control" id="">
                  <option {{ Request::input('limit') == 10 ? 'selected' : '' }}>10</option>  
                  <option {{ Request::input('limit') == 20 ? 'selected' : '' }}>20</option>  
                  <option {{ Request::input('limit') == 50 ? 'selected' : '' }}>50</option>  
                  <option {{ Request::input('limit') == 100 ? 'selected' : '' }}>100</option>  
                </select>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i></button>             
                <a href="{{ url('export_data'.Auth::user()->fullRequestUi()) }}" target="blank" class="btn btn-success">Export Excel</a>             
              </form>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Lengkap</th>
                  <th>Asal Sekolah</th>
                  <th>No HP</th>
                  <th>Tanggal Daftar</th>
                  <th>Pembayaran</th>
                  <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($siswa as $k=>$item)
                  <tr>
                    <td>{{ $k+1 }}</td>
                    <td>{{ $item->nama_lengkap }}</td>
                    <td>{{ $item->nama_sekolah_asal }}</td>
                    <td>
                      @php
                          $no_hp = preg_replace('/[^0-9]/', '', $item->no_hp);
                      @endphp
                      @if (substr($no_hp,0,2) == "08")
                      <a class="sent_wa" data-id="{{ $item->id }}" target="blank" href="http://wa.me/62{{ substr($no_hp,1) }}?text=Berdasarkan%20keputusan%20Panitia%20Peserta%20Didik%20Baru%20SMP%20FUTUHIYYAH%20Mranggen%20Demak%2Cdengan%20ini%20memutuskan%20bahwa%3A%0D%0A%0D%0ANama%20%3A%20{{ str_replace(' ','%20',$item->nama_lengkap) }}%0D%0AAsal%20Sekolah%20%3A%20{{ str_replace(' ','%20',$item->nama_sekolah_asal) }}%0D%0A%3D%3D%2ADITERIMA%2A%3D%3D%0D%0A%0D%0ASebagai%20siswa%2Fsiswi%20SMP%20FUTUHIYYAH%20Mranggen%20Demak%20pada%20tahun%20pelajaran%202022%2F2023.%20%0D%0ASelanjutnya%20dimohon%20segera%20melakukan%20daftar%20ulang%20dan%20melengkapi%20berkas%20pendaftaran.  Untuk rincian daftar ulang dapat dilihat di link berikut https://ppdbv2.smpfutuhiyyah.sch.id/public/daful_smpf_22.jpeg">{{ $item->no_hp }}</a>
                      @elseif (substr($no_hp,0,2) == "62")
                      <a class="sent_wa" data-id="{{ $item->id }}" target="blank" href="http://wa.me/{{ $no_hp }}?text=Berdasarkan%20keputusan%20Panitia%20Peserta%20Didik%20Baru%20SMP%20FUTUHIYYAH%20Mranggen%20Demak%2Cdengan%20ini%20memutuskan%20bahwa%3A%0D%0A%0D%0ANama%20%3A%20{{ str_replace(' ','%20',$item->nama_lengkap) }}%0D%0AAsal%20Sekolah%20%3A%20{{ str_replace(' ','%20',$item->nama_sekolah_asal) }}%0D%0A%3D%3D%2ADITERIMA%2A%3D%3D%0D%0A%0D%0ASebagai%20siswa%2Fsiswi%20SMP%20FUTUHIYYAH%20Mranggen%20Demak%20pada%20tahun%20pelajaran%202022%2F2023.%20%0D%0ASelanjutnya%20dimohon%20segera%20melakukan%20daftar%20ulang%20dan%20melengkapi%20berkas%20pendaftaran.  Untuk rincian daftar ulang dapat dilihat di link berikut https://ppdbv2.smpfutuhiyyah.sch.id/public/daful_smpf_22.jpeg">{{ $item->no_hp }}</a>
                      @endif
                      @if ($item->sent)
                      <small class="label pull-right bg-green">{{ $item->sent }}</small> 
                      @endif
                    </td>
                    <td>{{ Carbon\Carbon::parse($item->created_at)->isoFormat('DD MMMM Y') }}</td>
                    <td>
                      @if($item->pendaftaran)<small class="label pull-right bg-green">Pendaftaran</small>@endif
                      @if($item->daftar_ulang)<small class="label pull-right bg-blue">{{ rupiah(intval($item->daftar_ulang)) }}</small>@endif
                    </td>
                    <td>
                      @if(!$item->pendaftaran)<button type="button" class="btn btn-info btn-xs btn_pendaftaran" data-toggle="modal" data-target="#pendaftaran" data-url="{{ url('pendaftaran', $item->id) }}"><i class="fa fa-check-circle"></i> Pendaftaran</button>@endif
                      @if($item->pendaftaran)<button class="btn btn-success btn-xs btn_daftar_ulang" data-toggle="modal" data-target="#daftarUlang" data-url="{{ url('daftar_ulang', $item->id) }}"><i class="fa fa-check-circle"></i> Daftar Ulang</button>@endif
                      <a href="{{ url('cetak_form', $item->id) }}" target="blank" class="btn btn-warning btn-xs" title="Cetak Form"> <i class="fa fa-print"></i></a>
                      <a href="{{ url('detil', $item->id) }}" class="btn btn-info btn-xs" title="Detail"> <i class="fa fa-eye"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              {{ $siswa->links('pagination::bootstrap-4') }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('js')
<script>
  $('.sent_wa').click(function(){
    $.get("{{ url('sent_wa') }}/"+$(this).data('id'), function (data) {
      setTimeout(
      function() 
      {
        window.location.reload();
        //do something special
      }, 1000);
    })
  })
  $('.btn_pendaftaran').click(function(){
    $('.btn_konfirmasi').attr('href', $(this).data('url'))
  })
  $('.btn_daftar_ulang').click(function(){
    $('.form_konfirmasi').attr('action', $(this).data('url'))
  })
  $('.btn_konfirmasi').click(function(){
    setTimeout(
      function() 
      {
        window.location.reload();
        //do something special
      }, 2000);
  })
</script>
@endsection