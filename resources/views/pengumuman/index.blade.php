@extends('layouts.admin')
@section('content')
    	
	<section class="content-header">
	  <h1>
		Pengumuman
		{{-- <small>Control panel</small> --}}
        <a href="{{route('pengumuman.create')}}" id='' class="btn btn-sm btn-success" title="Tambah Data"><i class="fa fa-plus-circle"></i> Tambah Data</a>
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Pengumuman</li>
	  </ol>
	</section>

	<!-- Main content -->
    <section class="content">

      <div class='callout' style="margin: 0px; padding: 0px 30px 0px 15px;">
        @if (session('simpan'))
        <div class="row">
            <div class="alert alert-success alert-dismissible" role="alert">
                <h4 class="alert-heading">Berhasil!</h4>
                {{ session('simpan') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Judul</th>
                  <th>Isi</th>
                  <th>Created At</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
					@foreach ($data as $k=>$item)
					<tr>
						<td>{{ $k+1 }}</td>
						<td>{{ $item->judul }}</td>
						<td>{{ $item->isi }}</td>
						<td>{{ $item->created_at }}</td>
						<td>
              <form action="{{route('pengumuman.destroy', $item->id)}}" method="post">
                <a href="{{route('pengumuman.edit', $item->id)}}" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>
                @csrf
                @method('delete')
                {{-- <button type="button" class="btn_delete_modal_submit btn btn-xs btn-warning" data-toggle="modal" data-target="#deleteModal" data-url_modal="{{route('pengumuman.destroy', $item->id)}}"><i class="fa fa-trash"></i></button> --}}
                <button type="submit" class="btn btn-xs btn-warning"><i class="fa fa-trash"></i></button>
              </form>
						</td>
					</tr>
					@endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection